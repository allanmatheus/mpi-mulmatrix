#!/bin/bash

workdir="${1:-.}"
headers_file="${2:-headers.csv}"

sizes="1 10 20 40 80 160 320"

cat "$headers_file"
for i in $sizes ; do
    grep -rni elapsed "$workdir/${i}-procs" | cut -d' ' -f2,5 | \
            xargs -I {} echo "$i {}" | tr ' ' ','
done
