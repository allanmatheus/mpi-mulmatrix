#!/usr/bin/env python

import argparse

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--font-size', type=int, default=16,
                        help='Text font size')
    parser.add_argument('--dataset', required=True,
                        help='CSV dataset file to plot')
    return parser.parse_args()

def aggregate_time(df, nprocs, nrows, etime, agg_fn=('mean', 'std')):
    sizes = df[nrows].unique()
    cfg_nprocs = df[nprocs].unique()

    etime_mean = {}
    for p in cfg_nprocs:
        sub_df = df[df[nprocs] == p]
        etime_mean[p] = sub_df.groupby(nrows).mean()[etime].values

    return sizes, cfg_nprocs, etime_mean

def calc_speedup(tm):
    speedups = {}
    for p in tm.keys():
        speedups[p] = tm[1] / tm[p]
    return speedups

def plot_speedup(sizes, speedups):
    nprocs = list(speedups.keys())

    by_size = pd.DataFrame(speedups).T.to_dict('list')

    plt.plot(nprocs, nprocs)
    for i, _ in enumerate(sizes):
        plt.plot(nprocs, by_size[i])

    plt.legend(['Linear speedup'] + [f'{s}X{s}' for s in sizes])
    plt.xlabel('Número de processos')
    plt.ylabel('Speedup')
    plt.title('Speedup em multiplicação de matrizes quadradas')
    plt.grid(axis='y')

def plot_aggregated_time(sizes, cfg_nprocs, etime_mean):
    for _, y in etime_mean.items():
        plt.plot(sizes, y)

    plt.legend(cfg_nprocs)
    plt.xlabel('Tamanho da matriz')
    plt.ylabel('Tempo decorrido (segundos)')
    plt.title('Multiplicação de matrizes quadradas')
    plt.grid(axis='y')

if __name__ == '__main__':
    args = parse_args()
    matplotlib.rcParams.update({'font.size': args.font_size})

    dataset_df = pd.read_csv(args.dataset)
    nprocs, nrows, etime = dataset_df.columns

    agg_time = aggregate_time(dataset_df, nprocs, nrows, etime)
    plot_aggregated_time(*agg_time)

    plt.show()
    plt.cla()

    speedups = calc_speedup(agg_time[2])
    plot_speedup(agg_time[0], speedups)

    plt.show()
