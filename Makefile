.SILENT:

CC       := cc
#CFLAGS   := -std=c99 -pedantic -Wall -Wextra -Werror #-fopenmp -g -O4 ## for GCC
CFLAGS   := #-O 4 ## for Cray
CPPFLAGS :=
LD       := ${CC}
LDLIBS   :=
LDFLAGS  := #-fopenmp -g

PROGS := bin/genmatrix bin/mulmatrix

genmatrix_SRCS = src/genmatrix.c
mulmatrix_SRCS = src/matrix.c \
		 src/matrix_io.c \
		 src/mulmatrix.c

genmatrix_OBJS = ${genmatrix_SRCS:.c=.o}
mulmatrix_OBJS = ${mulmatrix_SRCS:.c=.o}

OBJS = ${genmatrix_OBJS} ${mulmatrix_OBJS}

all: ${PROGS}

bin/genmatrix: ${genmatrix_OBJS}
	@echo "CCLD $@"
	@mkdir -p bin
	${LD} ${LDFLAGS} -o $@ $^ ${LDLIBS}

bin/mulmatrix: ${mulmatrix_OBJS}
	@echo "CCLD $@"
	@mkdir -p bin
	${LD} ${LDFLAGS} -o $@ $^ ${LDLIBS}

.c.o:
	@echo "CC $@"
	${CC} ${CPPFLAGS} ${CFLAGS} -c -o $@ $<

clean:
	rm -f ${PROGS} ${OBJS}

.PHONY: all clean
