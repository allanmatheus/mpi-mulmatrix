#include <stdio.h>

#include <unistd.h>
#include <getopt.h>

#include <mpi.h>

#include "matrix.h"

#define DEBUG_INFO 0

int is_close(double x, double y, double eps);

void test_ax(void);

void multiply_matrices(const char *A_file, const char *B_file,
		const char *C_file);

void print_usage(const char *argv0);

void print_help(void);

int main(int argc, char **argv)
{
	int rt = 0;
	const char *A_file = NULL;
	const char *B_file = NULL;
	const char *C_file = NULL;

	MPI_Init(&argc, &argv);

	int op;
	while ((op = getopt(argc, argv, "ha:b:c:")) != -1) {
		switch (op) {
		case 'a':
			A_file = optarg;
			break;

		case 'b':
			B_file = optarg;
			break;

		case 'c':
			C_file = optarg;
			break;

		case 'h':
			print_usage(argv[0]);
			print_help();
			goto exit;

		default:
			print_usage(argv[0]);
			rt = 1;
			goto exit;
		}
	}

	if (A_file == NULL || B_file == NULL) {
		print_usage(argv[0]);
		rt = 1;
		goto exit;
	}

	multiply_matrices(A_file, B_file, C_file);

exit:
	MPI_Finalize();
	return rt;
}

void print_usage(const char *argv0)
{
	fprintf(stderr, "%s [-h] -a PATH -b PATH [-c PATH]\n", argv0);
}

void print_help(void)
{
	fprintf(stderr, "  -h   Display this help message.\n"
			"  -a   Read the firs matrix from this file.\n"
			"  -b   Read the second matrix from this file.\n"
			"  -c   Write the result to this file.\n"
			"       Defaults to stdout.\n");
}

void multiply_matrices(const char *A_file, const char *B_file,
		const char *C_file)
{
	const int root = 0;
	const int spray = 1;
	MPI_Comm comm = MPI_COMM_WORLD;

	int rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	FILE *rand_in = NULL;
	if (rank == root) {
		rand_in = fopen(A_file, "r");
		if (rand_in == NULL) {
			perror("fopen");
			MPI_Abort(comm, 1);
		}
	}

	struct matrix A;
	int err = matrix_from_file(&A, rand_in, spray, root, comm);
	if (err) {
		fprintf(stderr, "ERROR: Load rand.mat in A\n");
		MPI_Abort(comm, -err);
	}

	if (rank == root) {
		fclose(rand_in);
	}

	FILE *id_in = NULL;
	if (rank == root) {
		id_in = fopen(B_file, "r");
		if (id_in == NULL) {
			perror("fopen");
			MPI_Abort(comm, 1);
		}
	}

	struct matrix B;
	err = matrix_from_file(&B, id_in, !spray, root, comm);
	if (err) {
		fprintf(stderr, "ERROR: Load id.mat in B\n");
		MPI_Abort(comm, -err);
	}

	if (rank == root) {
		fclose(id_in);
	}

	struct matrix C;
	err = matrix_init_slices(&C, A.total_nrows, B.ncols, root, comm);
	if (err) {
		fprintf(stderr, "ERROR: Alloc C\n");
		MPI_Abort(comm, -err);
	}

#if DEBUG_INFO
	if (rank == root) {
		fprintf(stderr, "A (%d, %d): [", A.nrows, A.ncols);
		for (int i = 0; i < size; i++) {
			fprintf(stderr, "<%d,%d>, ", A.displs[i], A.counts[i]);
		}
		fprintf(stderr, "\b\b]\n");

		fprintf(stderr, "C (%d, %d): [", C.nrows, C.ncols);
		for (int i = 0; i < size; i++) {
			fprintf(stderr, "<%d,%d>, ", C.displs[i], C.counts[i]);
		}
		fprintf(stderr, "\b\b]\n");
	}
#endif /* DEBUG_INFO */

	err = matrix_mul(&A, &B, &C);
	if (err) {
		fprintf(stderr, "ERROR: Mul C = AB\n");
		MPI_Abort(comm, -err);
	}

	FILE *out = NULL;
	if (rank == root) {
		out = C_file == NULL ? stdout : fopen(C_file, "w");
		if (out == NULL) {
			perror("fopen");
			MPI_Abort(comm, 1);
		}
	}

	err = matrix_to_file(&C, out);
	if (err) {
		fprintf(stderr, "ERROR: Store C\n");
		MPI_Abort(comm, -err);
	}

	if (rank == root) {
		fclose(out);
	}

	matrix_free(&A);
	matrix_free(&B);
	matrix_free(&C);
}

int is_close(double x, double y, double eps)
{
	const double dt = (x > y) ? x - y : y - x;
	return dt <= eps;
}

void test_ax(void)
{
	MPI_Comm comm = MPI_COMM_WORLD;

	int rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	struct matrix A;
	int err = matrix_init_slices(&A, 3, 2, 0, comm);
	if (err) {
		fprintf(stderr, "ERROR: Alloc A\n");
		MPI_Abort(comm, -err);
	}
	if (!rank) {
		A.data[0] = 1; A.data[1] = 0;
		A.data[2] = 0; A.data[3] = 1;
		A.data[4] = 5; A.data[5] = 5;
	}

	err = matrix_spray(&A);
	if (err) {
		fprintf(stderr, "ERROR: Split A by rows\n");
		MPI_Abort(comm, -err);
	}

	struct matrix x;
	err = matrix_init(&x, 2, 1, 0, comm);
	if (err) {
		fprintf(stderr, "ERROR: Alloc x\n");
		MPI_Abort(comm, -err);
	}

	if (!rank) {
		x.data[0] = 3;
		x.data[1] = 3;
	}

	err = matrix_replicate(&x);
	if (err) {
		fprintf(stderr, "ERROR: Replicate x\n");
		MPI_Abort(comm, -err);
	}

	struct matrix y;
	err = matrix_init_slices(&y, 3, 1, 0, comm);
	if (err) {
		fprintf(stderr, "ERROR: Alloc y\n");
		MPI_Abort(comm, -err);
	}

	if (!rank) {
		fprintf(stderr, "[");
		for (int i = 0; i < size; i++) {
			fprintf(stderr, "<%d,%d>, ", y.displs[i], y.counts[i]);
		}
		fprintf(stderr, "\b\b]\n");
	}

#if DEBUG_INFO
	for (int i = 0; i < size; i++) {
		if (rank == i) {
			fprintf(stderr, "### rank=%d ###\n", rank);
			fprintf(stderr, "A:\n");
			matrix_print(&A, stderr);

			fprintf(stderr, "x:\n");
			matrix_print(&x, stderr);

			fprintf(stderr, "y:\n");
			matrix_print(&y, stderr);
		}
		MPI_Barrier(comm);
	}
#endif /* DEBUG_INFO */

	err = matrix_mul(&A, &x, &y);
	if (err) {
		fprintf(stderr, "ERROR: Mul y = Ax\n");
		MPI_Abort(comm, -err);
	}

#if DEBUG_INFO
	for (int i = 0; i < size; i++) {
		if (rank == i) {
			fprintf(stderr, "### rank=%d ###\n", rank);
			fprintf(stderr, "A:\n");
			matrix_print(&A, stderr);

			fprintf(stderr, "x:\n");
			matrix_print(&x, stderr);

			fprintf(stderr, "y:\n");
			matrix_print(&y, stderr);
		}
		MPI_Barrier(comm);
	}
#endif /* DEBUG_INFO */

	if (!rank) {
		double eps = 0.001;
		if (!is_close(y.data[0], 3.0, eps)) {
			fprintf(stderr, "1 x 3 + 0 x 3\n");
			MPI_Abort(comm, -err);
		}

		if (!is_close(y.data[1], 3.0, eps)) {
			fprintf(stderr, "0 x 3 + 1 x 3\n");
			MPI_Abort(comm, -err);
		}

		if (!is_close(y.data[2], 30.0, eps)) {
			fprintf(stderr, "5 x 3 + 5 x 3\n");
			MPI_Abort(comm, -err);
		}
	}

	matrix_free(&A);
	matrix_free(&x);
	matrix_free(&y);
}
