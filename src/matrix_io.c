#include <stdio.h>

#include <mpi.h>

#include "matrix.h"

void matrix_print(struct matrix *self, FILE *out)
{
	if (self == NULL || self->nrows < 1 || self->ncols < 1) {
		return;
	}

	for (int i = 0; i < self->nrows; i++) {
		fprintf(out, "|");
		for (int j = 0; j < self->ncols; j++) {
			fprintf(out, "%lf ", self->data[self->ncols*i+j]);
			if (j + 1 == self->ncols) {
				fprintf(out, "\b|\n");
			}
		}
	}
}

int matrix_from_file(struct matrix *self, FILE *in, int spray, int root,
		MPI_Comm comm)
{
	int rank;
	MPI_Comm_rank(comm, &rank);

	if (self == NULL) {
		return -1;
	}

	/* nrows, ncols */
	int matrix_size[2];
	matrix_size[0] = -1;
	matrix_size[1] = -1;

	if (rank == root) {
		int n = fscanf(in, "%d %d", &matrix_size[0], &matrix_size[1]);
		if (n != 2) {
			return -1;
		}
	}

	int rt = MPI_Bcast(matrix_size, 2, MPI_INT, root, comm);
	if (rt != 0) {
		return -rt;
	}

	rt = spray ?
		matrix_init_slices(self, matrix_size[0], matrix_size[1], root,
				comm) :
		matrix_init(self, matrix_size[0], matrix_size[1], root, comm);
	if (rt == -1) {
		return -1;
	}

	if (rank == root) {
		for (int i = 0; i < matrix_size[0]; i++) {
			for (int j = 0; j < matrix_size[1]; j++) {
				const int d = matrix_size[1] * i + j;
				if (fscanf(in, "%lf", &self->data[d]) != 1) {
					return -1;
				}
			}
		}
	}

	rt = spray ?  matrix_spray(self) : matrix_replicate(self);
	if (rt == -1) {
		return -1;
	}

	return 0;
}

int matrix_to_file(struct matrix *self, FILE *out)
{
	if (self == NULL) {
		return -1;
	}

	int rank;
	MPI_Comm_rank(self->comm, &rank);

	if (rank != self->root) {
		return 0;
	}

	int rt = fprintf(out, "%d %d\n", self->nrows, self->ncols);
	if (rt == -1) {
		return -1;
	}

	for (int i = 0; i < self->nrows; i++) {
		for (int j = 0; j < self->ncols; j++) {
			const int is_last = j == self->ncols - 1;
			const char *pattern = is_last ? "%lf\n" : "%lf ";

			const int d = self->ncols * i + j;

			rt = fprintf(out, pattern, self->data[d]);
			if (rt == -1) {
				return -1;
			}
		}
	}

	return 0;
}
