#include <stdio.h>
#include <stdlib.h>

#include <mpi.h>

#include "matrix.h"

int matrix_init(struct matrix *self, int nrows, int ncols, int root,
		MPI_Comm comm)
{
	if (self == NULL || nrows < 1 || ncols < 1) {
		return -1;
	}

	self->data = calloc(nrows * ncols, sizeof(double));
	if (self->data == NULL) {
		perror("calloc");
		return -1;
	}

	self->nrows = nrows;
	self->ncols = ncols;

	self->root = root;
	self->comm = comm;

	self->displs = NULL;
	self->counts = NULL;
	self->total_nrows = nrows;
	return 0;
}

int matrix_replicate(struct matrix *self)
{
	return -MPI_Bcast(self->data, self->nrows * self->ncols, MPI_DOUBLE,
			self->root, self->comm);
}

int matrix_init_slices(struct matrix *self, int nrows, int ncols, int root,
		MPI_Comm comm)
{
	if (self == NULL || nrows < 1 || ncols < 1) {
		return -1;
	}

	int rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	if (nrows < size) {
		fprintf(stderr, "Too much processes! Try up to %d\n", nrows);
		return -1;
	}

	self->counts = calloc(size, sizeof(int));
	if (self->counts == NULL) {
		perror("calloc");
		return -1;
	}

	self->displs = calloc(size, sizeof(int));
	if (self->displs == NULL) {
		perror("calloc");
		return -1;
	}

	const int div = nrows / size;
	const int rem = nrows % size;

	for (int i = 0; i < size; i++) {
		self->displs[i] = i * div * ncols;
		self->counts[i] = (i == size-1 ? div + rem : div) * ncols;
	}

	self->nrows = rank == root ? nrows : self->counts[rank] / ncols;
	self->ncols = ncols;
	self->total_nrows = nrows;

	const int matrix_size = self->nrows * self->ncols;
	self->data = calloc(matrix_size, sizeof(double));
	if (self->data == NULL) {
		perror("calloc");
		return -1;
	}

	self->root = root;
	self->comm = comm;

	return 0;
}

int matrix_spray(struct matrix *self)
{
	int rank;
	MPI_Comm_rank(self->comm, &rank);

	if (rank == self->root) {
		return -MPI_Scatterv(self->data, self->counts,
				self->displs, MPI_DOUBLE, MPI_IN_PLACE, 0,
				MPI_DOUBLE, self->root, self->comm);

	} else {
		const int c = self->counts[rank];
		return -MPI_Scatterv(NULL, NULL, NULL, MPI_DOUBLE,
				self->data, c, MPI_DOUBLE, self->root,
				self->comm);
	}
}

void matrix_free(struct matrix *self)
{
	if (self != NULL) {
		if (self->data != NULL) {
			free(self->data);
			self->data = NULL;
		}

		self->nrows = 0;
		self->ncols = 0;

		if (self->displs != NULL) {
			free(self->displs);
			self->displs = NULL;
		}

		if (self->counts != NULL) {
			free(self->counts);
			self->counts = NULL;
		}
		self->total_nrows = 0;

		self->root = -1;
		self->comm = NULL;
	}
}

int matrix_mul(struct matrix *self, struct matrix *other,
		struct matrix *result)
{
	if (self == NULL || other == NULL || result == NULL) {
		return -1;
	}

	if (self->ncols != other->nrows || self->nrows < 1 || self->ncols < 1
			|| other->ncols < 1) {
		return -1;
	}

	if (self->nrows != result->nrows || other->ncols != result->ncols) {
		return -1;
	}

	if (self->comm != other->comm || other->comm != result->comm) {
		return -1;
	}

	int rank;
	MPI_Comm_rank(self->comm, &rank);

	const int m = (self->counts == NULL) ? self->nrows
		: (self->counts[rank] / self->ncols);
	const int n = other->ncols;
	const int q = self->ncols;

	double *a = self->data;
	double *b = other->data;
	double *c = result->data;

//#pragma omp parallel for
	for (int i = 0; i < m; i++) {
//#pragma omp parallel for
		for (int j = 0; j < n; j++) {
			double s = 0.0;
//#pragma omp parallel for reduction(+:s)
			for (int k = 0; k < q; k++) {
				s += a[q*i+k] * b[n*k+j];
			}
			c[n*i+j] = s;
		}
	}

	if (rank == self->root) {
		return -MPI_Gatherv(MPI_IN_PLACE, 0, MPI_DOUBLE,
				result->data, result->counts, result->displs,
				MPI_DOUBLE, result->root, result->comm);

	} else {
		return -MPI_Gatherv(result->data, result->counts[rank],
				MPI_DOUBLE, NULL, NULL, NULL, MPI_DOUBLE,
				result->root, result->comm);
	}
}
