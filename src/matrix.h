#ifndef MATRIX_MPI_H
#define MATRIX_MPI_H

#include <stdio.h>

#include <mpi.h>

struct matrix {
	int nrows;
	int ncols;
	double *data;

	int root;
	MPI_Comm comm;

	int *displs;
	int *counts;
	int total_nrows;
};

/* Creatioin */

int matrix_init(struct matrix *self, int nrows, int ncols, int root,
		MPI_Comm comm);

int matrix_init_slices(struct matrix *self, int nrows, int ncols, int root,
		MPI_Comm comm);

void matrix_free(struct matrix *self);

/* Arithmetic operations */

int matrix_mul(struct matrix *self, struct matrix *other,
		struct matrix *result);

/* Data decomposition/distribution */

int matrix_spray(struct matrix *self);

int matrix_replicate(struct matrix *self);

/* IO operations */

void matrix_print(struct matrix *self, FILE *out);

/* IO (root-only) operations */

int matrix_from_file(struct matrix *self, FILE *in, int spray, int root,
		MPI_Comm comm);

int matrix_to_file(struct matrix *self, FILE *out);

#endif /* MATRIX_MPI_H */
